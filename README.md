...I did a lot of improvements since but didn't have the time to update the readme, it's outdated, I'll make changes soon!

To see the older commits, look at the [GeekQuote](https://bitbucket.org/DdZ-Fred/geekquote) repo.

# ***Welcome*** #

This little app was originally an exercice to learn the Android Development basics (create activities, intents and a one object database).
It's a kind of Notes/Memo app but, even the course and the exercice over, the app was useless so I decided to add some improvements. It was also a way for me to learn more.
I renamed the app to DdZ-Notes! why DdZ ? hmmm ...long story!

![Portrait - Main Screen with List objects_med.png](https://bitbucket.org/repo/rA9gK5/images/3917880709-Portrait%20-%20Main%20Screen%20with%20List%20objects_med.png)

# ***Requirements*** #

- [Parchment](https://github.com/EmirWeb/parchment) library. It's a ProjectLibrary that allows you, among other things, to create horizontal listviews. Read the [Wiki](https://github.com/EmirWeb/parchment/wiki/Eclipse-Tutorial) for the configuration


# ***Ideas and future improvements*** #

- The app should be "more" integrated(communications with other native apps like Calendar, Mail, Maps...)

- The app is a little "static", it should show more responsiveness, show the user it's responding to his actions with simple animations(moving objects, color changing...) 


- **3rd screen / QuoteActivity**
     * Have to work on the layout and its content, I haven't done that much

# ***Screenshots*** #

![Portrait - NewListDialog_med.png](https://bitbucket.org/repo/rA9gK5/images/2184408208-Portrait%20-%20NewListDialog_med.png)

![Portrait - QuoteListActivity with Quote objects_med.png](https://bitbucket.org/repo/rA9gK5/images/4199057910-Portrait%20-%20QuoteListActivity%20with%20Quote%20objects_med.png)
package com.ddzsoft.geekquote;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;

public class SplashScreen extends Activity {
	
	private LinearLayout mContainer;
	
	private AnimatorSet mAnimSet;
	private ObjectAnimator mFadeIn;
	private ObjectAnimator mBlinking;
	
	private ObjectAnimator mScaleX;
	private ObjectAnimator mScaleY;
	private AnimatorSet mScaleDisparition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		mContainer = (LinearLayout) findViewById(R.id.container);
		
		mFadeIn = ObjectAnimator.ofFloat(mContainer, "alpha", 0.0f, 1.0f);
		mFadeIn.setDuration(1500);
		
		mBlinking = ObjectAnimator.ofFloat(mContainer, "alpha", 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);
		mBlinking.setDuration(300);
		
		mScaleX = ObjectAnimator.ofFloat(mContainer, "scaleX", 1.0f, 0.0f);
		mScaleY = ObjectAnimator.ofFloat(mContainer, "scaleY", 1.0f, 0.0f);
		mScaleDisparition = new AnimatorSet();
		mScaleDisparition.setDuration(200);
		mScaleDisparition.playTogether(mScaleX, mScaleY);
		
		
		mAnimSet = new AnimatorSet();
		mAnimSet.playSequentially(mFadeIn, mBlinking, mScaleDisparition);
		mAnimSet.setInterpolator(new AccelerateDecelerateInterpolator());
		mAnimSet.setStartDelay(200);
		mAnimSet.start();
		mAnimSet.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {}
			
			@Override
			public void onAnimationRepeat(Animator animation) {}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				Intent intent = new Intent(getApplicationContext(), QuoteListTopActivity.class);
				startActivity(intent);
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});
		
	}
	
}

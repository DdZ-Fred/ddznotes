package com.ddzsoft.geekquote;

import java.util.ArrayList;

import com.ddzsoft.geekquote.dao.MHelp;

import android.app.Application;
import android.util.DisplayMetrics;
import android.view.ViewConfiguration;

public class DdZNotesApplication extends Application {

	private static DisplayMetrics dpMetrics;
	
	private static boolean ThumbnailsChecked = false;
	private static boolean menuButtonAvailable = false;
	@SuppressWarnings("unused")
	private static boolean sQLOpeningAnimationLaunch= false; //QL = Quote List
	public static ArrayList<Boolean> sAnimationLaunchedInQLActivities;
	
	@Override
	public void onCreate() {
		super.onCreate();
		initSingletons();
		dpMetrics = getResources().getDisplayMetrics();
		menuButtonAvailable = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
		sAnimationLaunchedInQLActivities = new ArrayList<Boolean>();
		
	}

	// Only visible to the package and all subclasses
	protected void initSingletons(){
		
		// Init UIL(Universal Image Library)
//		RoundedBitmapDisplayer rbd = new RoundedBitmapDisplayer(20);
//		DisplayImageOptions imgOptions = new DisplayImageOptions.Builder()
//		.cacheInMemory(false)
//		.displayer(rbd)
//		.imageScaleType(ImageScaleType.EXACTLY)
//		.bitmapConfig(Bitmap.Config.RGB_565)
//		.showImageOnLoading(drawable)  HAVE TO CREATE A DRAWABLE
//		.build();
//		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
//		.defaultDisplayImageOptions(imgOptions)
//		.writeDebugLogs()
//		.build();	
//		ImageLoader.getInstance().init(config);
		
		// Init DB
		MHelp.getInstance(getApplicationContext());
	}

	public static boolean isThumbnailsChecked() {
		return ThumbnailsChecked;
	}

	public static void setThumbnailsChecked(boolean thumbnailsChecked) {
		ThumbnailsChecked = thumbnailsChecked;
	}

	public static DisplayMetrics getDpMetrics() {
		return dpMetrics;
	}
	
	public static void setDpMetrics(DisplayMetrics dpMetrics) {
		DdZNotesApplication.dpMetrics = dpMetrics;
	}

	public static boolean isMenuButtonAvailable() {
		return menuButtonAvailable;
	}

	public static void setMenuButtonAvailable(boolean hasMenuButton) {
		DdZNotesApplication.menuButtonAvailable = hasMenuButton;
	}	
}

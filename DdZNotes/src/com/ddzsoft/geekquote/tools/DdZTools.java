package com.ddzsoft.geekquote.tools;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

public class DdZTools {

	public static void hideSoftKeyboard(Activity act) {
		if (act.getCurrentFocus() != null) {
			InputMethodManager imm = (InputMethodManager) act
					.getApplicationContext().getSystemService(
							Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(act.getCurrentFocus()
					.getWindowToken(), 0);
		}
	}
	
	public static void showSoftKeyboard(Activity act){
		if (act.getCurrentFocus() != null) {
			InputMethodManager imm = (InputMethodManager) act
					.getApplicationContext().getSystemService(
							Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(act.getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
		}
	}

	public static String truncate(String aStr) {
		if (aStr.length() > 10) {
			String truncStr = TextUtils.substring(aStr, 0, 10);
			Log.d("DDZ", "Truncated list name: " + truncStr);
			return truncStr;
		} else {
			return aStr;
		}
	}
	
	public static void DebugScreenSize(Activity act) {
		switch (act.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK) {

		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			Log.d("CONFIG", "SCREEN SIZE: SMALL");
			break;
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			Log.d("CONFIG", "SCREEN SIZE: NORMAL");
			break;
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			Log.d("CONFIG", "SCREEN SIZE: LARGE");
			break;
		case Configuration.SCREENLAYOUT_SIZE_XLARGE:
			Log.d("CONFIG", "SCREEN SIZE: XLARGE");
			break;
		default:
			Log.d("CONFIG", "SCREEN SIZE: ??????");
			break;
		}

	}
	
}

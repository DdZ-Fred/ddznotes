package com.ddzsoft.geekquote.dialog;

import java.util.ArrayList;

import org.lucasr.twowayview.TwoWayView;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.ddzsoft.geekquote.R;
import com.ddzsoft.geekquote.adapter.ImgGalleryAdapter2;

public class NewListDialogFragment2 extends DialogFragment {

	private static final String IMG_URI_PREFIX = "file://";
	
	OnQLAdded mCallback;

	private View mainLayout;
	private TextView dialogCustomTitle;
	
	private EditText nameInput;
	private TextView nameTxtView;
	
	private EditText detailsInput;
	
	private TextView colorTxtView;
	private TableRow backgroundColorPicker;
	
	private ImageView selectedImage;
	private boolean imgSelected;
	private int imgResId;
	private ArrayList<Integer> mResImgIds;
	
	private int colorDrawId;
	private boolean mColorSelected;
	private View yellow;
	private View rose;
	private View orange;
	private View green;
	private View blue;
	private View red;
	
	private TextView galleryTitleTxtView;
	private TwoWayView imageGallery;
	private ImgGalleryAdapter2 imgGalleryAdapter2;

	

	public NewListDialogFragment2() {
		this.imgSelected = false;
	}
	
	public interface OnQLAdded {
		public void onQLAdded(String name, String details, int resImg, int bColor);
		public void onQLAdded(String name, String details, String userImg, int bColor);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnQLAdded) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnQLAdded");
		}

	}
	
	public void initBackgroundIds() {
		mResImgIds = new ArrayList<Integer>();

		mResImgIds.add(R.drawable.b_phone_2);
		mResImgIds.add(R.drawable.b_headphones);
		mResImgIds.add(R.drawable.b_book);
		mResImgIds.add(R.drawable.b_shopping_cart);
		mResImgIds.add(R.drawable.b_tools);
		mResImgIds.add(R.drawable.b_briefcase);
	}
	
	public void initColorViews(View parentView){
		yellow = parentView.findViewById(R.id.yellow);
		yellow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_yellow;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_yellow_no_padding);
				mColorSelected = true;
				
			}
		});
		rose = parentView.findViewById(R.id.rose);
		rose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_rose;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_rose_no_padding);
				mColorSelected = true;
				
			}
		});
		orange = parentView.findViewById(R.id.orange);
		orange.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_orange;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_orange_no_padding);
				mColorSelected = true;
				
			}
		});
		green = parentView.findViewById(R.id.green);
		green.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_green;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_green_no_padding);
				mColorSelected = true;
				
			}
		});
		blue = parentView.findViewById(R.id.blue);
		blue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_blue;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_blue_no_padding);
				mColorSelected = true;
				
			}
		});
		red = parentView.findViewById(R.id.red);
		red.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				colorDrawId = R.drawable.main_ql_listview_item_selector_red;
				selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_red_no_padding);
				mColorSelected = true;
				
			}
		});
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder blder = new AlertDialog.Builder(getActivity());
		View view = getActivity().getLayoutInflater().inflate(
				R.layout.new_ql_dialog, null);
	
		mainLayout = view.findViewById(R.id.addQLMainLayout);
		dialogCustomTitle = (TextView) getActivity().getLayoutInflater().inflate(R.layout.new_ql_dialog_custom_title, null);
		
		nameTxtView = (TextView) view.findViewById(R.id.nameView);
		nameInput = (EditText) view.findViewById(R.id.nameInput);
//		detailsInput = (EditText) view.findViewById(R.id.detailsInput);
		
		
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
			colorTxtView = (TextView) view.findViewById(R.id.backgroundColorTitle);
		}else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			backgroundColorPicker = (TableRow) view.findViewById(R.id.backgroundColorPicker);
		}

		galleryTitleTxtView = (TextView) view.findViewById(R.id.galleryTitleTxtView);
		imageGallery = (TwoWayView) view
				.findViewById(R.id.imageGallery);
		
		selectedImage = (ImageView) view.findViewById(R.id.selectedBackground);
		
		if(savedInstanceState != null){
			int imgId = savedInstanceState.getInt("selectedImgResId");
			int colorId = savedInstanceState.getInt("selectedColorId");
			
			if(imgId != 0){
				imgResId = imgId;
				selectedImage.setImageResource(imgResId);
				imgSelected = true;
			}
			
			if(colorId != 0){
				colorDrawId = colorId;
				setColorSelectedImage();
				mColorSelected = true;
			}
			

		}

		initBackgroundIds();
		initColorViews(view);
		
		imgGalleryAdapter2 = new ImgGalleryAdapter2(getActivity(), mResImgIds);
		imageGallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				imgResId = mResImgIds.get(position);
				selectedImage.setImageResource(imgResId);
				imgSelected = true;
			}
		});
		

		imageGallery.setAdapter(imgGalleryAdapter2);
		
		blder.setView(view)
//				.setTitle(R.string.grid_act_addQL_diag_title)
				.setCustomTitle(dialogCustomTitle)
				.setPositiveButton(android.R.string.ok, null)
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();

							}
						});

		final AlertDialog addQLDiag = blder.create();
		addQLDiag.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				Button b = addQLDiag.getButton(AlertDialog.BUTTON_POSITIVE);
				b.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						String nameInputText = nameInput.getText().toString();

						if (nameInputText.isEmpty() || (nameInputText == null)
								|| !imgSelected || !mColorSelected) {
							if (nameInputText.isEmpty()
									|| (nameInputText == null)) {
								LaunchScalingAnimation(nameTxtView);
								nameInput.requestFocus();
							}
							if (!imgSelected) {
								LaunchScalingAnimation(galleryTitleTxtView);
							}
							if (!mColorSelected) {
								if (colorTxtView != null) {
									LaunchScalingAnimation(colorTxtView);
								} else {
									LaunchScalingAnimation(backgroundColorPicker);
								}
							}
						} else {
							mCallback.onQLAdded(nameInput.getText().toString(),
									"", imgResId, colorDrawId);
							addQLDiag.dismiss();
						}

					}
				});

			}
		});
		
		//Set my Fullscreen style to the my DialogFragment. NOT SURE IT'S REALLY USEFUL!!
		setStyle(DialogFragment.STYLE_NORMAL, R.style.my_dialog);
		return addQLDiag;
	}
	
	private void setColorSelectedImage() {
		switch (colorDrawId) {
		case R.drawable.main_ql_listview_item_selector_yellow:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_yellow_no_padding);
			break;
		case R.drawable.main_ql_listview_item_selector_rose:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_rose_no_padding);
			break;
		case R.drawable.main_ql_listview_item_selector_orange:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_orange_no_padding);
			break;
		case R.drawable.main_ql_listview_item_selector_green:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_green_no_padding);
			break;
		case R.drawable.main_ql_listview_item_selector_blue:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_blue_no_padding);
			break;
		case R.drawable.main_ql_listview_item_selector_red:
			selectedImage.setBackgroundResource(R.drawable.main_ql_listview_item_default_red_no_padding);
			break;

		default:
			break;
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		Dialog d = getDialog();

		d.getWindow().setGravity(Gravity.CENTER);

		forceWrapContent(mainLayout);
	}
	
	// Travel through the parent views(parent, then parent of the parent...)
	// to set WRAP_CONTENT to it, and this, until Parent is null
	protected void forceWrapContent(View v){
		View current = v;
		
		do {
			ViewParent parent = current.getParent();
			
			if(parent != null){
				try {
					current = (View) parent;
				} catch (ClassCastException e) {
					break;
				}
				
				current.getLayoutParams().width = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
			}
		} while (current.getParent() != null);
		
		current.requestLayout();
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle savedData) {
		super.onSaveInstanceState(savedData);
		
		if(imgSelected){
			savedData.putInt("selectedImgResId", imgResId);
		}
		if(mColorSelected){
			savedData.putInt("selectedColorId", colorDrawId);
		}
		
	}	

	private void LaunchScalingAnimation(View v){
		ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1.0f, 1.5f, 1.0f);
		ObjectAnimator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1.0f, 1.5f, 1.0f);
		scaleX.setDuration(400);
		scaleY.setDuration(400);
		AnimatorSet animSet = new AnimatorSet();
		animSet.play(scaleX).with(scaleY);
		animSet.start();
	}

	public TextView getNameTxtView() {
		return nameTxtView;
	}

	public void setNameTxtView(TextView nameTxtView) {
		this.nameTxtView = nameTxtView;
	}

	public TextView getGalleryTitleTxtView() {
		return galleryTitleTxtView;
	}

	public void setGalleryTitleTxtView(TextView galleryTitleTxtView) {
		this.galleryTitleTxtView = galleryTitleTxtView;
	}

	public boolean isImgSelected() {
		return imgSelected;
	}

	public void setImgSelected(boolean imgSelected) {
		this.imgSelected = imgSelected;
	}

	public EditText getNameInput() {
		return nameInput;
	}

	public void setNameInput(EditText nameInput) {
		this.nameInput = nameInput;
	}

	public EditText getDetailsInput() {
		return detailsInput;
	}

	public void setDetailsInput(EditText detailsInput) {
		this.detailsInput = detailsInput;
	}

	public ImageView getSelectedImage() {
		return selectedImage;
	}

	public void setSelectedImage(ImageView selectedImage) {
		this.selectedImage = selectedImage;
	}

	public int getSelectedColorDrawable() {
		return colorDrawId;
	}

	public void setSelectedColorDrawable(int selectedColorDrawable) {
		this.colorDrawId = selectedColorDrawable;
	}

	public boolean isColorSelected() {
		return mColorSelected;
	}

	public void setColorSelected(boolean colorSelected) {
		mColorSelected = colorSelected;
	}

}

package com.ddzsoft.geekquote.dialog;

import com.ddzsoft.geekquote.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ListDeletionConfirmation extends DialogFragment {

	OnQLDeleted mCallback;
	int posQLToDelete;
	

	public ListDeletionConfirmation(){}
	
	public interface OnQLDeleted {
		public void onQLDeleted(int pos);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		if(savedInstanceState != null){
			posQLToDelete = savedInstanceState.getInt("qlPos");
		}else{
			posQLToDelete = getArguments().getInt("qlPos");
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.main_ql_act_del_confirm_txt)
		.setPositiveButton(R.string.yes_button_txt, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mCallback.onQLDeleted(posQLToDelete);
				dialog.dismiss();
				
			}
		})
		.setNegativeButton(R.string.no_button_txt, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		
		return builder.create();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			mCallback = (OnQLDeleted) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnQLDeleted");
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedData) {
		savedData.putInt("qlPos", posQLToDelete);
	}
	
}

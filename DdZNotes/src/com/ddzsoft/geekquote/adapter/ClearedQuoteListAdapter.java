package com.ddzsoft.geekquote.adapter;

import java.util.ArrayList;

import com.ddzsoft.geekquote.DdZNotesApplication;
import com.ddzsoft.geekquote.QuoteListActivity;
import com.ddzsoft.geekquote.R;
import com.ddzsoft.geekquote.dao.MHelp;
import com.ddzsoft.geekquote.model.Quote;
import com.ddzsoft.geekquote.tools.DdZTools;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView.OnEditorActionListener;

public class ClearedQuoteListAdapter extends BaseAdapter {

	private static ArrayList<Quote> selectedQuotes = new ArrayList<Quote>();

	private static ArrayList<Quote> quotesList;
	private QuoteListActivity activity;
	private LayoutInflater li;
	private String quotesListName;
	private static boolean allSelected;

	public ClearedQuoteListAdapter(QuoteListActivity myActivity, ArrayList<Quote> list,
			String listName) {
		ClearedQuoteListAdapter.quotesList = list;
		this.activity = myActivity;
		this.quotesListName = listName;
		allSelected = false;

		li = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public LayoutInflater getLi() {
		return li;
	}

	public void setLi(LayoutInflater li) {
		this.li = li;
	}

	public static ArrayList<Quote> getQuotesList() {
		return quotesList;
	}

	public static void setQuotesList(ArrayList<Quote> quotesList) {
		ClearedQuoteListAdapter.quotesList = quotesList;
	}

	public QuoteListActivity getActivity() {
		return activity;
	}

	public void setActivity(QuoteListActivity activity) {
		this.activity = activity;
	}

	public static ArrayList<Quote> getSelectedQuotes() {
		return selectedQuotes;
	}

	public static void setSelectedQuotes(ArrayList<Quote> selectedQuotes) {
		ClearedQuoteListAdapter.selectedQuotes = selectedQuotes;
	}

	public String getQuotesListName() {
		return quotesListName;
	}

	public void setQuotesListName(String quotesListName) {
		this.quotesListName = quotesListName;
	}

	public static boolean areAllSelected() {
		return allSelected;

	}

	public static void setAllSelected(boolean allSelectedOrNot) {
		allSelected = allSelectedOrNot;
	}

	@Override
	public int getCount() {
		return quotesList.size();
	}

	@Override
	public Quote getItem(int position) {
		return quotesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Log.d("DDZ", "Parent(ListView) child count: " + parent.getChildCount());

		View v = convertView;
		Quote aQuote = this.getItem(position);

		CheckBox cb;
		EditText ev;

		// New view
		if (v == null) {

			v = li.inflate(R.layout.activity_quote_list_item, null);
			cb = (CheckBox) v.findViewById(R.id.selectCheckbox);
			ev = (EditText) v.findViewById(R.id.quoteTxtView);

			v.setTag(new QuoteViewHolder(cb, ev));
			
			// Existing view retrieved
		} else {

			QuoteViewHolder vh = (QuoteViewHolder) v.getTag();
			cb = vh.getCheckbox();
			ev = vh.getEditView();

		}

		if (aQuote != null) {

			// set the quote as a tag so we can retrieve quote info when needed
			ev.setTag(aQuote);
			ev.setText(aQuote.getStrQuote());

			cb.setTag(aQuote);
			cb.setChecked(aQuote.isChecked());

			cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					Quote q = (Quote) buttonView.getTag();

					if (isChecked) {
						q.setChecked(true);
						if (!selectedQuotes.contains(q)) {
							selectedQuotes.add(q);
						}

						Log.d("SELECTED QUOTES", "Quote #" + q.getId() + " - "
								+ q.getStrQuote() + " - ADDED");

					} else {
						q.setChecked(false);
						if (selectedQuotes.contains(q)) {
							selectedQuotes.remove(q);
						}

						Log.d("SELECTED QUOTES", "Quote #" + q.getId() + " - "
								+ q.getStrQuote() + " - REMOVED");
					}

					allSelectedChecker();

					buttonView.setTag(q);

					if(!activity.getAddQuoteItem().isActionViewExpanded()){
						activity.invalidateOptionsMenu();
					}


				}
			});
			cb.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// If ActionView is expanded and a click event occurs outside,
					// collapse the ActiobView
					collapseActionViewIfClickEvent();
					
				}
			});
			
			ev.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					// If ActionView is expanded and a click event occurs outside,
					// collapse the ActiobView
					collapseActionViewIfClickEvent();
					
					if(!v.isFocusable()){
						v.setFocusableInTouchMode(true);
					}
					
					if(!v.isFocused()){
						v.requestFocus();
					}
					
					DdZTools.showSoftKeyboard(activity);
					
				}
			});
			ev.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(!hasFocus){
						v.setFocusable(false);
						notifyDataSetChanged();
					}else{
						
						EditText editT = (EditText) v;
						editT.setSelection(editT.getText().length());
					}
				}
			});
			ev.setOnEditorActionListener(new OnEditorActionListener() {
				
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					
					Quote updatedQuote = (Quote)v.getTag();
					updatedQuote.setStrQuote(v.getText().toString());
					int pos = quotesList.indexOf(updatedQuote);
					quotesList.set(pos, updatedQuote);
					MHelp.QuoteDAO.updateQuote(updatedQuote);
					
					v.setFocusableInTouchMode(false);
					
					return false;
				}
			});
			
			if(DdZNotesApplication.sAnimationLaunchedInQLActivities.get(activity.getQLPosition())){
				v.setAlpha(1.0f);
			}

		}

		return v;
	}

	public static class QuoteViewHolder {
		private CheckBox checkbox;
		private EditText editView;

		public QuoteViewHolder() {
		}

		public QuoteViewHolder(CheckBox cb, EditText ev) {
			this.checkbox = cb;
			this.editView = ev;
		}

		public CheckBox getCheckbox() {
			return checkbox;
		}

		public void setCheckbox(CheckBox checkbox) {
			this.checkbox = checkbox;
		}

		public EditText getEditView() {
			return editView;
		}

		public void setEditView(EditText editView) {
			this.editView = editView;
		}

	}
	
	public void collapseActionViewIfClickEvent(){
		if(activity.getAddQuoteItem().isActionViewExpanded()){
			activity.launchCollapsingAnimation();
		}
	}

	public static void allSelectedChecker() {

		if (selectedQuotes.size() == quotesList.size()) {
			allSelected = true;
			Log.d("DDZ", "All quotes are selected - " + selectedQuotes.size()
					+ "/" + quotesList.size());
		} else {
			allSelected = false;
			Log.d("DDZ",
					"Not all quotes are selected - " + selectedQuotes.size()
							+ "/" + quotesList.size());
		}

		showDebugSelectedQuotes();
	}

	public static void showDebugSelectedQuotes() {

		String selectedQuotesString = "";
		for (Quote q2 : selectedQuotes) {
			selectedQuotesString += q2.getId() + " - " + q2.getStrQuote()
					+ ", ";
		}

		Log.d("DDZ", "Selected quotes: " + selectedQuotesString);

	}
	
}

package com.ddzsoft.geekquote.adapter;

import java.util.ArrayList;
import com.ddzsoft.geekquote.R;
import com.ddzsoft.geekquote.model.QuoteList;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

///////////////////////////////////////
/////////////IN PROGRESS///////////////
///////////////////////////////////////

public class QLAdapter extends BaseAdapter {

	private ArrayList<QuoteList> localQLCollection;
	private Context localCtx;
	private LayoutInflater li;

	public QLAdapter(Context ctx, ArrayList<QuoteList> qlCollection) {
		this.localCtx = ctx;
		this.localQLCollection = qlCollection;

		li = (LayoutInflater) localCtx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public ArrayList<QuoteList> getLocalQLCollection() {
		return localQLCollection;
	}

	public void setLocalQLCollection(ArrayList<QuoteList> localQLCollection) {
		this.localQLCollection = localQLCollection;
	}

	public Context getLocalCtx() {
		return localCtx;
	}

	public void setLocalCtx(Context localCtx) {
		this.localCtx = localCtx;
	}

	public LayoutInflater getLi() {
		return li;
	}

	public void setLi(LayoutInflater li) {
		this.li = li;
	}

	@Override
	public int getCount() {
		return localQLCollection.size();
	}

	@Override
	public Object getItem(int position) {
		return localQLCollection.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		QLViewHolder vh;

		if (v == null) {

			v = li.inflate(R.layout.main_ql_listview_item, null);
			vh = new QLViewHolder();
			vh.qlImg = (ImageView) v.findViewById(R.id.QlLViewItemImg);
			vh.qlName = (TextView) v.findViewById(R.id.QlLViewItemName);
			// vh.qlDetails = (TextView) v.findViewById(R.id.QlLViewItemDetails);
			vh.qlDate = (TextView) v.findViewById(R.id.QlLViewItemDate);

			v.setTag(vh);

		} else {

			vh = (QLViewHolder) v.getTag();

		}

		QuoteList ql = localQLCollection.get(position);
		if (ql != null) {

			switch (ql.getImgType()) {
			case QuoteList.RES_IMG:

				vh.qlImg.setBackgroundResource(ql.getResImg());

				break;
			case QuoteList.USER_IMG:

				Bitmap bmp = BitmapFactory.decodeFile(ql.getUserImg());
				// SET VALUES
				vh.qlImg.setImageBitmap(bmp);

				break;

			default:
				break;
			}

			v.setBackgroundResource(ql.getBackgroundColor());

			vh.qlName.setText(ql.getName());
			// vh.qlDetails.setText(ql.getDetails());
			// vh.qlDetails.setHorizontallyScrolling(true);
			vh.qlDate.setText(ql.getCreationDateStr());

		}

		return v;
	}

	public static class QLViewHolder {
		private ImageView qlImg;
		private TextView qlName;
		private TextView qlDetails;
		private TextView qlDate;

		public QLViewHolder() {
		}

		public QLViewHolder(ImageView img, TextView name, TextView details,
				TextView date) {
			this.qlImg = img;
			this.qlName = name;
			this.qlDetails = details;
			this.qlDate = date;
		}

		public ImageView getQlImg() {
			return qlImg;
		}

		public void setQlImg(ImageView qlImg) {
			this.qlImg = qlImg;
		}

		public TextView getQlName() {
			return qlName;
		}

		public void setQlName(TextView qlName) {
			this.qlName = qlName;
		}

		public TextView getQlDetails() {
			return qlDetails;
		}

		public void setQlDetails(TextView qlDetails) {
			this.qlDetails = qlDetails;
		}

		public TextView getQlDate() {
			return qlDate;
		}

		public void setQlDate(TextView qlDate) {
			this.qlDate = qlDate;
		}

	}

}

package com.ddzsoft.geekquote.adapter;

import java.util.ArrayList;

import com.ddzsoft.geekquote.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImgGalleryAdapter2 extends BaseAdapter {

	private Context mCtx;
	private LayoutInflater mLInflater;
	private ArrayList<Integer> mBackgroundIds;

	public ImgGalleryAdapter2() {
	}

	public ImgGalleryAdapter2(Context context, ArrayList<Integer> backgroundIds) {
		mCtx = context;
		mLInflater = (LayoutInflater) mCtx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mBackgroundIds = backgroundIds;

	}

	@Override
	public int getCount() {
		return mBackgroundIds.size();
	}

	@Override
	public Object getItem(int position) {
		return mBackgroundIds.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		GalleryViewHolder gvh;
		
		if (v == null) {

			v = mLInflater.inflate(R.layout.new_ql_dialog_img_lview_item, null);
			gvh = new GalleryViewHolder();
			gvh.image = (ImageView) v.findViewById(R.id.galleryImage);
			v.setTag(gvh);

		} else {
			gvh = (GalleryViewHolder) v.getTag();
		}
		
		int backgroundId = mBackgroundIds.get(position);

		gvh.image.setBackgroundResource(backgroundId);

		return v;
	}

	public static class GalleryViewHolder {

		private ImageView image;
		
		public GalleryViewHolder() {
		}

		public GalleryViewHolder(ImageView img) {
			image = img;
		}

		public ImageView getImage() {
			return image;
		}

		public void setImage(ImageView image) {
			this.image = image;
		}

	}

}

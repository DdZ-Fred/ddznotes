package com.ddzsoft.geekquote.dao;

import android.provider.BaseColumns;

// This class(Contract class) specifies the layout of the schema in a systematic and self-documenting way.
// It is also a container for constants that defines names, tables, columns, URIs, SQL Strings
// It gives you the ability to use these constants across all the classes in the same package.
// So we can have all the SQL CODE in ONE ONLY PLACE, easier to maintain!!
public final class GQCtract {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "geekquote.db";
	private static final String TEXT_TYPE = " TEXT";
	private static final String REAL_TYPE = " REAL"; // Floating point value
	private static final String INT_TYPE = " INTEGER";
	private static final String COMMA_SEP = ", ";

	// FOREIGN KEY CONSTRAINTS ARE DISABLED BY DEFAULT so...
	public static final String ENABLE_FOREIGN_KEYS = "PRAGMA foreign_keys=ON;";

	// If return 0, then support is OFF and have to be turned ON
	// If return 1, then support is ON, nothing to do!
	// If return NOTHING, then either SQLite version is older than 3.6.19
	// OR it was compiled with SQLITE_OMIT_FOREIGN_KEY
	// OR SQLITE_OMIT_TRIGGER is defined
	public static final String IS_FOREIGN_KEY_SUPPORTED = "PRAGMA foreign_keys";

	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	public GQCtract() {
	}

	public static abstract class QuoteEntry implements BaseColumns {

		public static final String TABLE_NAME = "quote";
		public static final String COL1_QUOTE_TEXT = "text";
		public static final String COL2_QUOTE_DATE = "date";
		public static final String COL3_QUOTE_RATING = "rating";
		public static final String COL4_QUOTE_LIST_ID = "qlistid";
		public static final String[] COLUMNS = { _ID, COL1_QUOTE_TEXT,
				COL2_QUOTE_DATE, COL3_QUOTE_RATING, COL4_QUOTE_LIST_ID };
		public static final String[] COLUMNS_NO_QLID = { _ID, COL1_QUOTE_TEXT,
				COL2_QUOTE_DATE, COL3_QUOTE_RATING };
		public static final String[] COLUMNS_NO_ID = { COL1_QUOTE_TEXT,
				COL2_QUOTE_DATE, COL3_QUOTE_RATING, COL4_QUOTE_LIST_ID };
		public static final String[] COLUMN_ID_ONLY = { _ID };

		public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
				+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ COL1_QUOTE_TEXT + TEXT_TYPE + COMMA_SEP + COL2_QUOTE_DATE
				+ TEXT_TYPE + COMMA_SEP + COL3_QUOTE_RATING + REAL_TYPE
				+ COMMA_SEP + COL4_QUOTE_LIST_ID + INT_TYPE + ")";
		public static final String DELETE_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;

	}

	public static abstract class QuoteListEntry implements BaseColumns {

		public static final String TABLE_NAME 				= "quote_list";
		public static final String COL1_NAME 				= "name";
		public static final String COL2_DETAILS 			= "details";
		public static final String COL3_CREATION_DATE 		= "date";
		public static final String COL4_IMG_TYPE 			= "img_type";
		public static final String COL5_RES_IMG 			= "res_img";
		public static final String COL6_USER_IMG 			= "user_img";
		public static final String COL7_BACKGROUND_COLOR 	= "background_color";
		public static final String IDX_NAME 				= "idxOnQLName";
		
		public static final String[] COLUMNS = { _ID,
												COL1_NAME,
												COL2_DETAILS,
												COL3_CREATION_DATE,
												COL4_IMG_TYPE,
												COL5_RES_IMG,
												COL6_USER_IMG,
												COL7_BACKGROUND_COLOR};
		
		public static final String[] COLUMNS_NO_ID = { COL1_NAME,
												COL2_DETAILS,
												COL3_CREATION_DATE,
												COL4_IMG_TYPE,
												COL5_RES_IMG,
												COL6_USER_IMG,
												COL7_BACKGROUND_COLOR};
		
		public static final String[] COLUMN_ID_ONLY = { _ID };

		public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
				+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ COL1_NAME + TEXT_TYPE + COMMA_SEP
				+ COL2_DETAILS + TEXT_TYPE + COMMA_SEP
				+ COL3_CREATION_DATE + TEXT_TYPE + COMMA_SEP
				+ COL4_IMG_TYPE + INT_TYPE + COMMA_SEP
				+ COL5_RES_IMG + INT_TYPE + COMMA_SEP
				+ COL6_USER_IMG + TEXT_TYPE + COMMA_SEP
				+ COL7_BACKGROUND_COLOR + INT_TYPE + ")";
		public static final String CREATE_NAME_INDEX = "CREATE UNIQUE INDEX IF NOT EXISTS "
				+ IDX_NAME + " ON " + TABLE_NAME + " (" + COL1_NAME + ")";
		public static final String DELETE_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;
		public static final String DELETE_NAME_INDEX = "DROP INDEX IF EXISTS "
				+ IDX_NAME;

	}

}

package com.ddzsoft.geekquote.dao;

import java.text.ParseException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ddzsoft.geekquote.dao.GQCtract.QuoteEntry;
import com.ddzsoft.geekquote.model.Quote;
import com.ddzsoft.geekquote.model.QuoteList;

// SQLiteOpenHelper provide easy db maintenance and management.
public class MHelp extends SQLiteOpenHelper {

	private volatile static MHelp mInstance;
	private static Context ctx;

	protected MHelp(Context aContext) {
		super(aContext, GQCtract.DATABASE_NAME, null, GQCtract.DATABASE_VERSION);
		ctx = aContext;
	}

	// Lazy initialization + Double-checked Locking
	public static MHelp getInstance(Context iContext) {
		if (mInstance == null) {
			synchronized (MHelp.class) {
				if (mInstance == null) {
					mInstance = new MHelp(iContext);
				}
			}
		}
		return mInstance;
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(GQCtract.QuoteEntry.CREATE_TABLE);
		db.execSQL(GQCtract.QuoteListEntry.CREATE_TABLE);
		db.execSQL(GQCtract.QuoteListEntry.CREATE_NAME_INDEX);

	}

	// Called when the database has been opened.The implementation should
	// check isReadOnly() before updating the database. This method is called
	// after
	// the database connection has been configured and after the database schema
	// has
	// been created, upgraded or downgraded as necessary. If the database
	// connection
	// must be configured in some way before the schema is created, upgraded, or
	// downgraded, do it in onConfigure(SQLiteDatabase) instead.
	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			db.execSQL(GQCtract.ENABLE_FOREIGN_KEYS);
		}
	}

	// Method is called during an upgrade of the database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(GQCtract.QuoteListEntry.DELETE_NAME_INDEX);
		db.execSQL(GQCtract.QuoteListEntry.DELETE_TABLE);
		db.execSQL(GQCtract.QuoteEntry.DELETE_TABLE);
		onCreate(db);

	}

	// ----------------------------------------------------------------//
	// -------------------- CRUD OPERATIONS----------------------------//
	// ----------------------------------------------------------------//

	// getWritableDatabase: Create and/or open a database that will be used for
	// reading
	// and writing. Once opened successfully, the database is cached, so you can
	// call
	// this method every time you need to write to the database. (Make sure to
	// call
	// close() when you no longer need the database.)
	public static abstract class QuoteDAO {

		public static Quote addQuote(String strQuote, long listId) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			Quote newQuote = new Quote(strQuote);

			ContentValues newQuoteC = new ContentValues();
			newQuoteC.put(QuoteEntry.COL1_QUOTE_TEXT, newQuote.getStrQuote());
			newQuoteC.put(QuoteEntry.COL2_QUOTE_DATE,
					newQuote.getCreationDateString());
			newQuoteC.put(QuoteEntry.COL3_QUOTE_RATING, newQuote.getRating());
			newQuoteC.put(GQCtract.QuoteEntry.COL4_QUOTE_LIST_ID, listId);

			newQuote.setId(db.insert(QuoteEntry.TABLE_NAME, null, newQuoteC));

			db.close();

			Log.d("DATABASE", "ADDING NEW QUOTE TO DB");

			return newQuote;

		}

		public static Quote getQuoteById(long quoteId) throws ParseException {

			SQLiteDatabase db = mInstance.getReadableDatabase();

			//
			Cursor cursor = db.query(QuoteEntry.TABLE_NAME,
					QuoteEntry.COLUMNS_NO_ID, // 2nd
					// param=
					// Strings
					// array
					// containing
					// the
					// columns
					// you're
					// interested
					// in
					" _ID = ?", // 3rd = WHERE clause, without the WHERE!!
					new String[] { String.valueOf(quoteId) }, // 4th =
																// String
																// array
																// containing
																// the
																// arguments
																// for the
																// WHERE
																// clause.
					null, // 5th = GROUP BY...
					null, // 6th = HAVING ...
					null); // 7th = ORDER BY...

			if (cursor.moveToFirst()) {

				Quote quoteRequested = new Quote(quoteId, cursor.getString(0),
						cursor.getString(1), cursor.getFloat(2),
						cursor.getLong(3));

				cursor.close();
				db.close();

				return quoteRequested;

			} else {

				cursor.close();
				db.close();

				return null;
			}

		}

		public static ArrayList<Quote> getAllQuotes() throws ParseException {

			ArrayList<Quote> localList = new ArrayList<Quote>();

			SQLiteDatabase db = mInstance.getReadableDatabase();

			// The second parameter is a String array containing the columns we
			// are
			// interested in!
			Cursor cursor = db.query(QuoteEntry.TABLE_NAME, QuoteEntry.COLUMNS,
					null, null, null, null, null);

			while (cursor.moveToNext()) {
				localList.add(new Quote(cursor.getLong(0), cursor.getString(1),
						cursor.getString(2), cursor.getFloat(3), cursor
								.getLong(4)));

			}

			cursor.close();
			db.close();

			return localList;
		}

		public static ArrayList<Quote> getAllQuotesFromQL(long QLId)
				throws ParseException {

			SQLiteDatabase db = mInstance.getReadableDatabase();
			ArrayList<Quote> QLItems = new ArrayList<Quote>();

			Cursor csr = db.query(GQCtract.QuoteEntry.TABLE_NAME,
					GQCtract.QuoteEntry.COLUMNS_NO_QLID,
					GQCtract.QuoteEntry.COL4_QUOTE_LIST_ID + " = ?",
					new String[] { String.valueOf(QLId) }, null, null, null);

			while (csr.moveToNext()) {
				QLItems.add(new Quote(csr.getLong(0), csr.getString(1), csr
						.getString(2), csr.getFloat(3), QLId));
			}

			csr.close();
			db.close();
			return QLItems;
		}

		public static int updateQuote(Quote updatedQuote) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			ContentValues newValues = new ContentValues();
			newValues.put(QuoteEntry.COL1_QUOTE_TEXT,
					updatedQuote.getStrQuote());
//			newValues.put(QuoteEntry.COL2_QUOTE_DATE,
//					updatedQuote.getCreationDateString());
			newValues.put(QuoteEntry.COL3_QUOTE_RATING,
					updatedQuote.getRating());

			// update return the number of lines affected!!
			int i = db.update(QuoteEntry.TABLE_NAME, newValues, " _ID = ?",
					new String[] { String.valueOf(updatedQuote.getId()) });

			db.close();

			return i;
		}

		public static int deleteQuote(Quote toDelete) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			// Like "update", delete return the number of rows affected!
			int i = db.delete(QuoteEntry.TABLE_NAME, "_ID = ?",
					new String[] { String.valueOf(toDelete.getId()) });

			db.close();

			return i;

		}
	}

	public static abstract class QuoteListDAO {

		public static QuoteList addQL(String QLName, String QLDetails,
				String userImg, int bColor) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			QuoteList newQL = new QuoteList(QLName, QLDetails, userImg, bColor);
			ContentValues newQLContent = new ContentValues();

			newQLContent
					.put(GQCtract.QuoteListEntry.COL1_NAME, newQL.getName());
			newQLContent.put(GQCtract.QuoteListEntry.COL2_DETAILS,
					newQL.getDetails());
			newQLContent.put(GQCtract.QuoteListEntry.COL3_CREATION_DATE,
					newQL.getCreationDateStr());
			newQLContent.put(GQCtract.QuoteListEntry.COL4_IMG_TYPE,
					newQL.getImgType());
			newQLContent.put(GQCtract.QuoteListEntry.COL6_USER_IMG,
					newQL.getUserImg());
			newQLContent.put(GQCtract.QuoteListEntry.COL7_BACKGROUND_COLOR,
					newQL.getBackgroundColor());

			newQL.setId(db.insert(GQCtract.QuoteListEntry.TABLE_NAME, null,
					newQLContent));

			db.close();
			return newQL;
		}

		public static QuoteList addQL(String QLName, String QLDetails,
				int resImg, int bColor) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			QuoteList newQL = new QuoteList(QLName, QLDetails, resImg, bColor);
			ContentValues newQLContent = new ContentValues();

			newQLContent
					.put(GQCtract.QuoteListEntry.COL1_NAME, newQL.getName());
			newQLContent.put(GQCtract.QuoteListEntry.COL2_DETAILS,
					newQL.getDetails());
			newQLContent.put(GQCtract.QuoteListEntry.COL3_CREATION_DATE,
					newQL.getCreationDateStr());
			newQLContent.put(GQCtract.QuoteListEntry.COL4_IMG_TYPE,
					newQL.getImgType());
			newQLContent.put(GQCtract.QuoteListEntry.COL5_RES_IMG,
					newQL.getResImg());
			newQLContent.put(GQCtract.QuoteListEntry.COL7_BACKGROUND_COLOR,
					newQL.getBackgroundColor());

			newQL.setId(db.insert(GQCtract.QuoteListEntry.TABLE_NAME, null,
					newQLContent));

			db.close();
			return newQL;

		}

		public static QuoteList getQLById(long QLId) throws ParseException {

			SQLiteDatabase db = mInstance.getReadableDatabase();

			Cursor csr = db.query(GQCtract.QuoteListEntry.TABLE_NAME,
					GQCtract.QuoteListEntry.COLUMNS_NO_ID, "_ID = ?",
					new String[] { String.valueOf(QLId) }, null, null, null);

			// One of the columns COL5 and COL6 can be null!!!!
			QuoteList aQL = new QuoteList(QLId, csr.getString(0),
					csr.getString(1), csr.getString(2), csr.getInt(3),
					csr.getInt(4), csr.getString(5), csr.getInt(6));

			csr.close();
			db.close();
			return aQL;
		}

		public static ArrayList<QuoteList> getAllQL() throws ParseException {

			SQLiteDatabase db = mInstance.getReadableDatabase();
			ArrayList<QuoteList> allQL = new ArrayList<QuoteList>();

			Cursor csr = db.query(GQCtract.QuoteListEntry.TABLE_NAME,
					GQCtract.QuoteListEntry.COLUMNS, null, null, null, null,
					null);

			// CAN RETURN NULL FIELD
			while (csr.moveToNext()) {
				allQL.add(new QuoteList(csr.getLong(0), csr.getString(1), csr
						.getString(2), csr.getString(3), csr.getInt(4), csr
						.getInt(5), csr.getString(6), csr.getInt(7)));
			}

			csr.close();
			db.close();
			return allQL;
		}

		public static int updateQL(QuoteList updatedQL) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			ContentValues newValues = new ContentValues();
			newValues.put(GQCtract.QuoteListEntry.COL1_NAME,
					updatedQL.getName());
			newValues.put(GQCtract.QuoteListEntry.COL2_DETAILS,
					updatedQL.getDetails());
			newValues.put(GQCtract.QuoteListEntry.COL3_CREATION_DATE,
					updatedQL.getCreationDateStr());
			newValues.put(GQCtract.QuoteListEntry.COL4_IMG_TYPE,
					updatedQL.getImgType());
			newValues.put(GQCtract.QuoteListEntry.COL5_RES_IMG,
					updatedQL.getResImg());
			newValues.put(GQCtract.QuoteListEntry.COL6_USER_IMG,
					updatedQL.getUserImg());
			newValues.put(GQCtract.QuoteListEntry.COL7_BACKGROUND_COLOR,
					updatedQL.getBackgroundColor());
			int i = db.update(GQCtract.QuoteListEntry.TABLE_NAME, newValues,
					"_ID = ?",
					new String[] { String.valueOf(updatedQL.getId()) });

			db.close();
			return i;
		}

		public static int deleteQL(QuoteList QLToDelete) {

			SQLiteDatabase db = mInstance.getWritableDatabase();

			int i = db.delete(GQCtract.QuoteListEntry.TABLE_NAME, "_ID = ?",
					new String[] { String.valueOf(QLToDelete.getId()) });

			db.close();
			return i;
		}

	}

}

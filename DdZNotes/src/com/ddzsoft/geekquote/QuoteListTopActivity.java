package com.ddzsoft.geekquote;

import java.text.ParseException;
import java.util.ArrayList;

import com.ddzsoft.geekquote.adapter.QLAdapter;
import com.ddzsoft.geekquote.dao.MHelp;
import com.ddzsoft.geekquote.dialog.ListDeletionConfirmation;
import com.ddzsoft.geekquote.dialog.ListDeletionConfirmation.OnQLDeleted;
import com.ddzsoft.geekquote.dialog.NewListDialogFragment2;
import com.ddzsoft.geekquote.model.QuoteList;
import com.ddzsoft.geekquote.tools.DdZTools;
import com.ddzsoft.geekquote.tools.TypefaceSpan;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


public class QuoteListTopActivity extends FragmentActivity implements com.ddzsoft.geekquote.dialog.NewListDialogFragment2.OnQLAdded, OnQLDeleted{
	
	private ArrayList<QuoteList> quoteListCollection = new ArrayList<QuoteList>();
	
	private QLAdapter  mQLAdapter;
	
	//TEST
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_ql_listview);
		
		DdZTools.DebugScreenSize(QuoteListTopActivity.this);
		
		SpannableString s = new SpannableString(getResources().getString(R.string.app_name));
		s.setSpan(new TypefaceSpan(this, "Wallpoet-Regular.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(s);
		
		try {
			quoteListCollection = MHelp.QuoteListDAO.getAllQL();
			initAnimationLaunchedInQLActivities();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		ListView qlListView = (ListView) findViewById(R.id.QLListView);
		mQLAdapter = new QLAdapter(getApplicationContext(),quoteListCollection);
		qlListView.setAdapter(mQLAdapter);
		qlListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				Intent intent = new Intent(getApplicationContext(),QuoteListActivity.class);
				intent.putExtra("listId", quoteListCollection.get(position).getId());
				intent.putExtra("QLPosition", position);
				// ListName is truncated to prevent size problem when showing the text in the actionBar
				intent.putExtra("QLName", DdZTools.truncate(quoteListCollection.get(position).getName()));
//				LaunchTransitionAnim(v);
				startActivity(intent);
			}
		});
		
		qlListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				ListDeletionConfirmation delDialog = new ListDeletionConfirmation();
				Bundle qlToDel = new Bundle();
				qlToDel.putInt("qlPos", position);
				delDialog.setArguments(qlToDel);
				delDialog.show(getSupportFragmentManager(), "GIDBIN");
				return false;
			}
		});
		
		Log.d("DDZ", "Display metrics: W=" + DdZNotesApplication.getDpMetrics().widthPixels + ", H=" + DdZNotesApplication.getDpMetrics().heightPixels);

	}
	
	private void initAnimationLaunchedInQLActivities(){
		for (int i = 0; i < quoteListCollection.size(); i++) {
			DdZNotesApplication.sAnimationLaunchedInQLActivities.add(false);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuInflater mi = getMenuInflater();
		mi.inflate(R.menu.top_main_menu, menu);
		
		MenuItem addQL = menu.findItem(R.id.addQL);
		addQL.setVisible(true);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.addQL:
			
			DialogFragment frag = new NewListDialogFragment2();
			frag.show(getSupportFragmentManager(), "MEGA2");
			
			return true;

		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onQLAdded(String name, String details, int resImg, int bColor) {
		QuoteList QL = MHelp.QuoteListDAO.addQL(name, details, resImg, bColor);
		quoteListCollection.add(QL);
		mQLAdapter.notifyDataSetChanged();
		DdZNotesApplication.sAnimationLaunchedInQLActivities.add(false);
		
	}
	@Override
	public void onQLAdded(String name, String details, String userImg, int bColor) {
		// TO UDPATE / NOT IN USE FOR THE MOMENT
	}
	
	@Override
	public void onQLDeleted(int pos) {
		MHelp.QuoteListDAO.deleteQL(quoteListCollection.get(pos));
		quoteListCollection.remove(pos);
		DdZNotesApplication.sAnimationLaunchedInQLActivities.remove(pos);
		mQLAdapter.notifyDataSetChanged();
	}
}
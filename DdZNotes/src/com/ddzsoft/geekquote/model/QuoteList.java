package com.ddzsoft.geekquote.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class QuoteList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
	
	public static final int RES_IMG = 0;
	public static final int USER_IMG = 1;
	
	private long id;
	private String name;
	private String details;
	private Date creationDate;
	
	private int imgType;
	private int resImg;
	private String userImg;
	private int backgroundColor;
	
	// To use when User want to use images provided
	public QuoteList(String aName, String detailsStr, int resImg, int bColor){
		this.name = aName;
		this.details = detailsStr;
		this.creationDate = new Date();
		this.imgType = RES_IMG;
		this.resImg = resImg;
		this.backgroundColor = bColor;
	}
	
	// To use when User want to use his own images
	public QuoteList(String aName, String detailsStr, String userImg, int bColor){
		this.name = aName;
		this.details = detailsStr;
		this.creationDate = new Date();
		this.imgType = USER_IMG;
		this.userImg = userImg;
		this.backgroundColor = bColor;
	}
	
	public QuoteList(long anId, String aName, String detailsStr, String cDate, int imgType, int resImg, String userImg, int bColor) throws ParseException{
		this.id = anId;
		this.name = aName;
		this.details = detailsStr;
		this.creationDate = fmt.parse(cDate);
		this.imgType = imgType;
		
		if(imgType == RES_IMG){
			this.resImg = resImg;
		}
		if(imgType == USER_IMG){
			this.userImg = userImg;
		}
		this.backgroundColor = bColor;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public String getCreationDateStr(){
		return fmt.format(this.creationDate);
	}

	public int getImgType() {
		return imgType;
	}

	public void setImgType(int imgType) {
		this.imgType = imgType;
	}

	public int getResImg() {
		return resImg;
	}

	public void setResImg(int resImg) {
		this.resImg = resImg;
	}

	public String getUserImg() {
		return userImg;
	}

	public void setUserImg(String userImg) {
		this.userImg = userImg;
	}

	public int getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
}

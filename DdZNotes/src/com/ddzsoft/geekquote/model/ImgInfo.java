package com.ddzsoft.geekquote.model;

import java.io.Serializable;

public class ImgInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String imgData;

	public ImgInfo() {
	}

	public ImgInfo(long mId, String data) {
		this.id = mId;
		this.imgData = data;
	}

	public long getId() {
		return id;
	}

	public void setId(long mId) {
		this.id = mId;
	}

	public String getImgData() {
		return imgData;
	}

	public void setImgData(String imgData) {
		this.imgData = imgData;
	}

	@Override
	public boolean equals(Object o) {
		boolean b = false;
		if(o instanceof ImgInfo){
			ImgInfo tmp = (ImgInfo)o;
			b= (this.getId() == tmp.getId());
		}
		return b;
	}
	
	

}

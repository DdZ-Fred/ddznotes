package com.ddzsoft.geekquote.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

// Parcel is not a general-purpose serialization mechanism.
// This class (and the corresponding Parcelable API for placing
// arbitrary objects into a Parcel) is designed as a high-performance
// IPC transport. As such, it is not appropriate to place any Parcel
// data in to persistent storage
public class Quote implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Locale represents a language/country/variant combination.
	// Locales are used to alter the presentation of information such as
	// numbers
	// or dates to suit the conventions in the region they describe.
	private static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy",
			Locale.ENGLISH);

	private long id;
	private String strQuote;
	private float rating;
	private Date creationDate;
	private long quoteListId;
	private boolean checked = false;

	public Quote() {
	}

	// private Quote(Parcel source){
	// Bundle parcelReturn = source.readBundle();
	//
	// strQuote = parcelReturn.getString("strQuote");
	// rating = parcelReturn.getFloat("rating");
	// creationDate = (Date)parcelReturn.getSerializable("creationDate");
	// Log.d("PARCEL_TO_QUOTE", "RE-CREATION OF THE QUOTE FROM THE PARCEL");
	// }

	public Quote(String quoteTxt) {
		this.strQuote = quoteTxt;
		this.rating = 0;
		this.creationDate = new Date();
	}

	public Quote(long quoteId, String quoteTxt, String date, Float aRating,
			long qListId) throws ParseException {
		this.id = quoteId;
		this.strQuote = quoteTxt;
		this.creationDate = format.parse(date);
		this.rating = aRating;
		this.quoteListId = qListId;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStrQuote() {
		return strQuote;
	}

	public void setStrQuote(String strQuote) {
		this.strQuote = strQuote;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationDateString() {
		return format.format(creationDate);
	}

	public long getQuoteListId() {
		return quoteListId;
	}

	public void setQuoteListId(long quoteListId) {
		this.quoteListId = quoteListId;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void toggleChecked() {
		this.checked = !checked;
	}

	@Override
	public String toString() {

		return strQuote;
	}

	
	//Method overridden for the ArrayList.contains() method to be more reliable!
	@Override
	public boolean equals(Object o) {
		boolean b = false;
		if(o instanceof Quote){
			Quote tmp = (Quote)o;
			b = (this.getId() == tmp.getId());
		}
		Log.d("DDZ", "Quote.equals called");
		return b;
	}
	

	// The class MUST HAVE a static field called CREATOR
	// implementing the interface.
	// It's to generate a instance of the object from a Parcel.
	// Parcel = Data Container
	// public static final Parcelable.Creator<Quote> CREATOR
	// = new Parcelable.Creator<Quote>() {
	//
	// @Override
	// public Quote createFromParcel(Parcel source) {
	// return new Quote(source);
	// }
	//
	// @Override
	// public Quote[] newArray(int size) {
	// return new Quote[size];
	// }
	//
	// };

	// Describe the kinds of special objects contained in this Parcelable's
	// marshalled representation.
	// @Override
	// public int describeContents() {
	// return 1;
	// }

	// This is here we define the data we want to include in the Parcel
	// Obviously, all the object parameters!!
	// @Override
	// public void writeToParcel(Parcel dest, int flags) {
	//
	//
	// Bundle quoteInfo = new Bundle();
	// quoteInfo.putString("strQuote", strQuote);
	// quoteInfo.putFloat("rating", rating);
	// quoteInfo.putSerializable("creationDate", creationDate);
	//
	// dest.writeBundle(quoteInfo);
	//
	// }

}

package com.ddzsoft.geekquote;

import java.text.ParseException;
import java.util.ArrayList;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.test.PerformanceTestCase;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.ddzsoft.geekquote.adapter.ClearedQuoteListAdapter;
import com.ddzsoft.geekquote.dao.MHelp;
import com.ddzsoft.geekquote.model.Quote;
import com.ddzsoft.geekquote.tools.DdZTools;
import com.ddzsoft.geekquote.tools.TypefaceSpan;

public class QuoteListActivity extends Activity {

	private static final int MY_ACTIVITY_CODE = 1;
	private static final int MY_ACTIVITY_CODE_2 = 2;

	// Variables relative to the ActionBar
	private boolean menuInitialized = false;
	private MenuItem delete;
	private MenuItem addQuoteItem;
	private EditText qInput;
	private View separator;
	private TextView cancelButton;

	// Expand animation variables
	private AnimatorSet expandAnimSet;
	
	private AnimatorSet editTextAnimSet;
	private ObjectAnimator editTxtTranslX;
	private ObjectAnimator editTxtFadeIn;
	
	private AnimatorSet separatorAnimSet;
	private ObjectAnimator sepTranslY;
	private ObjectAnimator sepAlpha;
	
	private AnimatorSet cancelAnimSet;
	private ObjectAnimator cancelTranslX;
	private ObjectAnimator cancelAlpha;
	
	// Collapse animation variables
	private ObjectAnimator collapseScalX;
	private ObjectAnimator collapseScalY;
	private AnimatorSet collapseAnimSet;

	private Animation quoteAnimation;
	private int count = 0;

	private ArrayList<Quote> quotesList = new ArrayList<Quote>();
	private ClearedQuoteListAdapter quotesAdapter;
	private int[] selectedQuotesPositions;
	private LinearLayout mainLayout;
	private ListView listView;
	private long QLId;
	private String QLName;
	private int QLPosition;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quote_list);
		
		// Display Up arrow/button to return to the parent activity
		getActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState != null) {
			QLId = savedInstanceState.getLong("savedListId");
			QLName = savedInstanceState.getString("savedListName");
			QLPosition = savedInstanceState.getInt("QLPosition");
			
		} else {
			Bundle listInfo = getIntent().getExtras();
			QLId = listInfo.getLong("listId");
			QLName = listInfo.getString("QLName");
			QLPosition = listInfo.getInt("QLPosition");
		}

		SpannableString s = new SpannableString(QLName);
		s.setSpan(new TypefaceSpan(this, "Wallpoet-Regular.ttf"), 0,
				s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		// SET ACTIVITY TITLE DYNAMICALLY
		setTitle(s);

		try {
			quotesList = MHelp.QuoteDAO.getAllQuotesFromQL(QLId);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		mainLayout = (LinearLayout) findViewById(R.id.myVerLayout);
		mainLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(addQuoteItem.isActionViewExpanded()){
					collapseActionViewIfClickEvent();
				}
			}
		});
		

		// Pass QLName for the user to locate himself in the App (Fil d'ariane
		// = BreadCrumb)
		quotesAdapter = new ClearedQuoteListAdapter(this, quotesList, QLName);

		listView = (ListView) findViewById(R.id.myQuotesListView);
		listView.setAdapter(quotesAdapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				collapseActionViewIfClickEvent();
			}
		});
	
		initializeAnimations();

		if (!DdZNotesApplication.sAnimationLaunchedInQLActivities.get(QLPosition)) {
			listView.post(new Runnable() {

				@Override
				public void run() {

					if (listView.getCount() > 0) {
						firstOpeningQuotesAnim(listView
								.getLastVisiblePosition());
					}
					DdZNotesApplication.sAnimationLaunchedInQLActivities.set(QLPosition, true);
				}
			});
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ClearedQuoteListAdapter.getSelectedQuotes().clear();
		ClearedQuoteListAdapter.setAllSelected(false);
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onDestroy called");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onPause called");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onRestart called");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onResume called");
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onStart called");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d("DDZ", "QuoteListActivity - " + QLName + " - onStop called");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {

		case MY_ACTIVITY_CODE:

			switch (resultCode) {
			case RESULT_OK:
				Bundle dataReturn = data.getExtras();
				Quote updatedQuote = (Quote) dataReturn
						.getSerializable("updatedQuote");
				int updatedQuotePosition = dataReturn
						.getInt("updatedQuotePosition");

				MHelp.QuoteDAO.updateQuote(updatedQuote);

				ClearedQuoteListAdapter.getQuotesList().set(
						updatedQuotePosition, updatedQuote);

				quotesAdapter.notifyDataSetChanged();
				break;

			case RESULT_CANCELED:

				break;

			default:
				break;
			}

			break;

		default:
			break;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (DdZNotesApplication.isMenuButtonAvailable()) {
			getMenuInflater().inflate(R.menu.action_bar, menu);
		} else {
			// TEMPORARY - ...at least there will be a menu
			getMenuInflater().inflate(R.menu.action_bar, menu);
		}

		delete = menu.findItem(R.id.delete);
		addQuoteItem = menu.findItem(R.id.add);
		addQuoteItem.setOnActionExpandListener(new OnActionExpandListener() {

			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
//				View v = item.getActionView();
				launchExpandingAnimation();
				
				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				invalidateOptionsMenu();
				return true;
			}

		});

		View v = addQuoteItem.getActionView();
		qInput = (EditText) v.findViewById(R.id.quoteInput);
		
		// Validation listener
		qInput.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				String qInputContent = qInput.getText().toString();

				if(qInputContent.isEmpty() || (qInputContent == null)) {

					CharSequence text = "Type something first PLZ";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();
					return true;

				} else {
					addQuote(MHelp.QuoteDAO.addQuote(qInputContent, QLId));
					qInput.setText("");

					launchCollapsingAnimation();
					return false;
				}

			}
		});

		if (ClearedQuoteListAdapter.getSelectedQuotes().size() == 0) {
			delete.setVisible(false);
		} else {
			if (!delete.isVisible()) {
				delete.setVisible(true);
			}
		}
		
		separator = v.findViewById(R.id.actionViewItemsSeparator);
		cancelButton = (TextView) v.findViewById(R.id.cancelAdding);
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				launchCollapsingAnimation();
				
			}
		});

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.delete:

			deleteSelectedQuotes();
			Log.d("DDZ", "Activity quotesList size: " + quotesList.size());
			Log.d("DDZ", "ListView/Adapter getCount: " + listView.getCount());
			invalidateOptionsMenu();
			return true;

		case R.id.add:

			//
			addQuoteItem.expandActionView();
			return true;

		case R.id.omSelectAll:

			if (!ClearedQuoteListAdapter.areAllSelected()) {

				selectAllQuotes();
				invalidateOptionsMenu();
			}

			return true;

		case R.id.omUnselectAll:

			unselectAllQuotes();
			invalidateOptionsMenu();

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	public ArrayList<Quote> getQuotesList() {
		return quotesList;
	}

	public void setQuotesList(ArrayList<Quote> quotesList) {
		this.quotesList = quotesList;
	}

	public ClearedQuoteListAdapter getQuotesAdapter() {
		return quotesAdapter;
	}

	public void setQuotesAdapter(ClearedQuoteListAdapter quotesAdapter) {
		this.quotesAdapter = quotesAdapter;
	}

	public static int getMyActivityCode() {
		return MY_ACTIVITY_CODE;
	}

	public static int getMyActivityCode2() {
		return MY_ACTIVITY_CODE_2;
	}

	public ListView getListView() {
		return listView;
	}

	public void setListView(ListView listView) {
		this.listView = listView;
	}

	public long getQLId() {
		return QLId;
	}

	public void setQLId(long qLId) {
		QLId = qLId;
	}

	public String getQLName() {
		return QLName;
	}

	public void setQLName(String qLName) {
		QLName = qLName;
	}

	public boolean isMenuInitialized() {
		return menuInitialized;
	}

	public void setMenuInitialized(boolean menuInitialized) {
		this.menuInitialized = menuInitialized;
	}

	public MenuItem getDelete() {
		return delete;
	}

	public void setDelete(MenuItem delete) {
		this.delete = delete;
	}

	public MenuItem getAddQuoteItem() {
		return addQuoteItem;
	}

	public void setAddQuoteItem(MenuItem addQuoteItem) {
		this.addQuoteItem = addQuoteItem;
	}

	public EditText getqInput() {
		return qInput;
	}

	public void setqInput(EditText qInput) {
		this.qInput = qInput;
	}

	public int getQLPosition() {
		return QLPosition;
	}

	public void setQLPosition(int qLPosition) {
		QLPosition = qLPosition;
	}

	
	private void collapseActionViewIfClickEvent() {
		if(addQuoteItem.isActionViewExpanded()){
			launchCollapsingAnimation();
		}
		
	}
	
	public Quote addQuote(Quote newQuote) {
		quotesList.add(newQuote);
		quotesAdapter.notifyDataSetChanged();
		ClearedQuoteListAdapter.allSelectedChecker();
		return newQuote;
	}

	public void selectAllQuotes() {

		for (Quote q : quotesList) {
			q.setChecked(true);
			if (!ClearedQuoteListAdapter.getSelectedQuotes().contains(q)) {
				ClearedQuoteListAdapter.getSelectedQuotes().add(q);
			}
		}
		quotesAdapter.notifyDataSetChanged();
		ClearedQuoteListAdapter.allSelectedChecker();

	}

	public void unselectAllQuotes() {
		for (Quote q : quotesList) {
			q.setChecked(false);
			if (ClearedQuoteListAdapter.getSelectedQuotes().contains(q)) {
				ClearedQuoteListAdapter.getSelectedQuotes().remove(q);
			}
		}
		quotesAdapter.notifyDataSetChanged();
		ClearedQuoteListAdapter.allSelectedChecker();
	}

	public void deleteSelectedQuotes() {

		for (Quote q : ClearedQuoteListAdapter.getSelectedQuotes()) {
			quotesList.remove(q);
			MHelp.QuoteDAO.deleteQuote(q);
		}
		quotesAdapter.notifyDataSetChanged();
		ClearedQuoteListAdapter.getSelectedQuotes().clear();
		ClearedQuoteListAdapter.setAllSelected(false);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {

		// Save UI state changes to the savedInstanceState.
		// This bundle will be passed to OnCreate if the process is killed and
		// restarted
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putLong("savedListId", QLId);
		savedInstanceState.putString("savedListName", QLName);
		savedInstanceState.putInt("QLPosition", QLPosition);

		// Saving postions/indexes of the selected quotes
		if (ClearedQuoteListAdapter.getSelectedQuotes().size() > 0) {
			selectedQuotesPositions = new int[ClearedQuoteListAdapter
					.getSelectedQuotes().size()];
			int i = 0;
			for (Quote q : ClearedQuoteListAdapter.getSelectedQuotes()) {
				selectedQuotesPositions[i] = quotesList.indexOf(q);
				i++;
			}
		}
		savedInstanceState.putIntArray("savedSelectedQuotes",
				selectedQuotesPositions);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState.getIntArray("savedSelectedQuotes") != null) {
			selectedQuotesPositions = savedInstanceState
					.getIntArray("savedSelectedQuotes");
			for (int index : selectedQuotesPositions) {
				quotesList.get(index).setChecked(true);
				ClearedQuoteListAdapter.getSelectedQuotes().add(
						quotesList.get(index));
			}
			quotesAdapter.notifyDataSetChanged();
			ClearedQuoteListAdapter.allSelectedChecker();
		}
	}

	private void initializeAnimations() {

		// ***************************************** //
		// ***********PROPERTY ANIMATIONS*********** //
		// ***************************************** //

		
		
		
		
		// %%%%%%% ACTIONVIEW EXPAND ANIMATION %%%%%%% //
		
		// EditText animation
		editTxtTranslX = new ObjectAnimator();
		editTxtTranslX.setFloatValues(-100f, 0f);
		editTxtTranslX.setPropertyName("translationX");
		editTxtTranslX.setInterpolator(new AccelerateDecelerateInterpolator());
		
		editTxtFadeIn = new ObjectAnimator();
		editTxtFadeIn.setFloatValues(0.0f, 1.0f);
		editTxtFadeIn.setPropertyName("alpha");

		editTextAnimSet = new AnimatorSet();
		editTextAnimSet.play(editTxtTranslX).with(editTxtFadeIn);
		editTextAnimSet.setDuration(500);
		
		
		// Separator animation
		sepTranslY = new ObjectAnimator();
		sepTranslY.setFloatValues(-200f, 0.0f);
		sepTranslY.setPropertyName("translationY");
		sepTranslY.setInterpolator(new BounceInterpolator());
		
		sepAlpha = new ObjectAnimator();
		sepAlpha.setFloatValues(0.0f, 1.0f);
		sepAlpha.setPropertyName("alpha");
		
		separatorAnimSet = new AnimatorSet();
		separatorAnimSet.setDuration(400);
		separatorAnimSet.play(sepTranslY).with(sepAlpha);
		
		
		
		// Cancel button(TextView) animation
		cancelTranslX = new ObjectAnimator();
		cancelTranslX.setFloatValues(200.0f, 0.0f);
		cancelTranslX.setPropertyName("translationX");
		cancelTranslX.setInterpolator(new AccelerateDecelerateInterpolator());
		
		cancelAlpha = new ObjectAnimator();
		cancelAlpha.setFloatValues(0.0f, 1.0f);
		cancelAlpha.setPropertyName("alpha");
		
		cancelAnimSet = new AnimatorSet();
		cancelAnimSet.setDuration(500);
		cancelAnimSet.play(cancelTranslX).with(cancelAlpha);
		
		expandAnimSet = new AnimatorSet();
		expandAnimSet.playSequentially(editTextAnimSet, separatorAnimSet, cancelAnimSet);
		expandAnimSet.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				qInput.requestFocus();
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				DdZTools.showSoftKeyboard(QuoteListActivity.this);
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {}
		});
		
		
		// %%%%%%% ACTIONVIEW COLLAPSE ANIMATION %%%%%%% //
		collapseScalX = new ObjectAnimator();
		collapseScalX.setFloatValues(1.0f, 0.008f);
		collapseScalX.setPropertyName("scaleX");
		collapseScalX.setDuration(500);
		
		collapseScalY = new ObjectAnimator();
		collapseScalY.setFloatValues(1.0f, 3.0f, 0.0f);
		collapseScalY.setPropertyName("scaleY");
		collapseScalY.setInterpolator(new OvershootInterpolator(0.8f));
		collapseScalY.setDuration(400);
		
		collapseAnimSet = new AnimatorSet();
		collapseAnimSet.play(collapseScalX).before(collapseScalY);
		collapseAnimSet.addListener(new AnimatorListener() {

			@Override
			public void onAnimationStart(Animator animation) {
				DdZTools.hideSoftKeyboard(QuoteListActivity.this);
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				addQuoteItem.collapseActionView();
				resetActionViewState();
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});

		// ****************************************** //
		// *************TWEEN ANIMATIONS************* //
		// ****************************************** //
		quoteAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.custom_anim_test);
		quoteAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {}

			@Override
			public void onAnimationRepeat(Animation animation) {}

			@Override
			public void onAnimationEnd(Animation animation) {
				while (count < listView.getLastVisiblePosition()) {
					listView.getChildAt(count++).startAnimation(quoteAnimation);
				}
			}
		});
	}


	private void resetActionViewState() {
			collapseScalX.reverse();
			collapseScalY.reverse();
	}

	public void launchExpandingAnimation(){
		
		editTextAnimSet.setTarget(qInput);
		separatorAnimSet.setTarget(separator);
		cancelAnimSet.setTarget(cancelButton);
		expandAnimSet.start();
	}
	
	public void launchCollapsingAnimation() {
		View v = addQuoteItem.getActionView();
		float pivotX = v.getWidth() / 2;
		float pivotY = v.getHeight() / 2;
		v.setPivotX(pivotX); // Expressed in pixels relative to the object's
								// left edge
		v.setPivotY(pivotY);

		collapseAnimSet.setTarget(v);
		collapseAnimSet.start();
	}

	private void firstOpeningQuotesAnim(int lastVisiblePosition) {

		int duration = 150;
		int delay = 200;
		float itemW = listView.getChildAt(0).getMeasuredWidth();
		float itemH = listView.getChildAt(0).getMeasuredHeight();

		AnimatorSet completeAnimation = new AnimatorSet();
		// AnimatorSet extends Animator so...
		ArrayList<Animator> animsToPlaySequentially = new ArrayList<Animator>();

		for (int i = 0; i <= lastVisiblePosition; i++) {

			// JUST REMEMBERED THE VALUES ARE SUPPOSED TO BE
			// IN Pixels relative to the object's left edge
			ObjectAnimator translateX = new ObjectAnimator();
			translateX.setPropertyName("translationX");
			translateX.setFloatValues(itemW, 0);
			ObjectAnimator translateY = new ObjectAnimator();
			translateY.setPropertyName("translationY");
			translateY.setFloatValues(-3 * itemH, 0);
			ObjectAnimator alpha = new ObjectAnimator();
			alpha.setPropertyName("alpha");
			alpha.setFloatValues(0.0f, 1.0f);

			AnimatorSet item = new AnimatorSet();
			item.setDuration(duration);
			item.playTogether(translateX, translateY, alpha);
			item.setTarget(listView.getChildAt(i));

			if (i == 0) {
				item.setStartDelay(delay);
			}
			animsToPlaySequentially.add(item);
		}

		completeAnimation.playSequentially(animsToPlaySequentially);
		completeAnimation.setInterpolator(new OvershootInterpolator(0.8f));
		completeAnimation.start();
	}

}
